#ifndef IMAGE_TRANSFORMER_IMAGE_PIXEL_H
#define IMAGE_TRANSFORMER_IMAGE_PIXEL_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

bool image_malloc(struct image *image, uint32_t width, uint32_t height);

void image_free(struct image *image);

#endif
