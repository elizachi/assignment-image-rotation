#ifndef UNPACKER_H
#define UNPACKER_H

#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE **in_file, const char *filepath, int mode);
bool close(FILE **out_file);

#endif
