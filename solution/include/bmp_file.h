#ifndef IMAGE_TRANSFORMER_BMP_FILE_H
#define IMAGE_TRANSFORMER_BMP_FILE_H

#include "../include/header.h"
#include "image_pixel.h"
#include <stdbool.h>
#include <stdio.h>

bool from_bmp(FILE* file, struct image* img);

bool to_bmp(FILE* file, struct image const* image);

#endif
