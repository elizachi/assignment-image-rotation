#ifndef IMAGE_TRANSFORMER_IMAGE_ROT_H
#define IMAGE_TRANSFORMER_IMAGE_ROT_H

struct image rotate(struct image const image);

#endif

