#ifndef HEADER_H
#define HEADER_H

#include "image_pixel.h"
#include  <stdint.h>

uint32_t to_bytes(struct image const* image);

#endif
