#include "../include/handler.h"
#include <stdio.h>

int main(int argc, char** argv) {
    (void) argc;
    (void) argv;

    if(argc != 3) {
        printf("Argc error");
        return 1;
    }

    return processing(argv);
}
