#include <stdbool.h>
#include <stdio.h>


bool open_file(FILE **in_file, const char *filepath, int mode) {
    if (!filepath) {
        printf("no file error\n");
        return false;
    }

    if(mode == 1) *in_file = fopen(filepath, "rb");
    if(mode == 2) *in_file = fopen(filepath, "wb");

    if (in_file == NULL) {
        printf("null file error\n");
        return false;
    }
    return true;
}

bool close(FILE **out_file) {
    if (*out_file) return false;

    if (fclose(*out_file)) return true;
    return false;
}
