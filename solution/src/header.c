#include "../include/header.h"
#include "../include/image_pixel.h"
#include <stdbool.h>
#include <stdint.h>

uint32_t to_bytes(struct image const* image) {
    uint8_t pad = ((image->width) * sizeof(struct pixel)) % 4;

    pad = (pad != 0) ? 4 - pad : pad;

    return sizeof(struct pixel) * image -> height * image -> width + pad * image -> height;
}
