#include "../include/bmp_file.h"
#include "../include/handler.h"
#include "../include/image_pixel.h"
#include "../include/image_rot.h"
#include "../include/unpacker.h"
#include <stdio.h>

void close_file(FILE *first, FILE *second) {
    close(&first);
    close(&second);
}

int processing(char **argv) {
    FILE *in_file = NULL;
    FILE *out_file = NULL;

    struct image in_image = {0};
    struct image out_image = {0};

    if(!open_file(&in_file, argv[1], 1)) {
        fprintf(stderr, "%s\n", "Open in_file error");
        return 2;
    }

    if(!(from_bmp(in_file, &in_image))){
        fprintf(stderr, "%s\n", "From_bmp error");
        close_file(in_file, out_file);
        return 3;
    }

    out_image = rotate(in_image);

    if(!open_file(&out_file, argv[2], 2)) {
        fprintf(stderr, "%s\n", "Open out_file error");
        return 2;
    }


    if(!to_bmp(out_file, &out_image)) {
        fprintf(stderr, "%s\n", "To_bmp error");
        close_file(in_file, out_file);
        image_free(&out_image);
    }

    image_free(&in_image);
    image_free(&out_image);

    close_file(in_file, out_file);

    fprintf(stdout, "%s\n", "Rotation was successful!");

    return 0;
}
