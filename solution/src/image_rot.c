#include "../include/image_pixel.h"
#include "../include/image_rot.h"
#include <stdint.h>

struct image adress_pixel(struct image aside, struct image const image) {
    for(size_t i = 0; i < image.height; i++) {
        for (size_t j = 0; j < image.width; j++) {

            aside.data[(j * aside.width) + (aside.width - i - 1)] =
                    image.data[i * image.width + j];

        }
    }
    return aside;
}

struct image rotate(struct image const image) {
    struct image aside;

    uint32_t width = image.height;
    uint32_t height = image.width;

    aside.width = width;
    aside.height = height;

    if(!image_malloc(&aside, width, height)) return aside;

    return adress_pixel(aside, image);
}
