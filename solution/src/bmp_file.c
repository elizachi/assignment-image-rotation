#include "../include/bmp_file.h"
#include "../include/header.h"
#include "../include/image_pixel.h"
#include <stdbool.h>
#include <stdio.h>

static const uint32_t BF_TYPE = 0x4D42;
static const uint32_t BI_SIZE = 40;
static const uint32_t BFRESERVED = 0;
static const uint32_t BI_PLANES = 1;
static const uint32_t BI_BIT_COUNT = 24;
static const uint32_t BI_COMPRESSION = 0;
static const uint32_t BI_X_PER_METER = 2834;
static const uint32_t BI_Y_PER_METER = 2834;
static const uint32_t BI_CLR_USED = 0;
static const uint32_t BI_CLR_IMP = 0;

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static struct bmp_header create_header(const struct image * image){
    struct bmp_header header = {0};
    header.bfType = BF_TYPE;

    header.bfileSize = sizeof(struct bmp_header) +
            to_bytes(image);

    header.bfReserved = BFRESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = image -> width;
    header.biHeight = image -> height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BIT_COUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = to_bytes(image);
    header.biXPelsPerMeter = BI_X_PER_METER;
    header.biYPelsPerMeter = BI_Y_PER_METER;
    header.biClrUsed = BI_CLR_USED;
    header.biClrImportant = BI_CLR_IMP;
    return header;
}

static bool is_valid(struct bmp_header const * header) {
    if(header -> biHeight > 0 && header -> biWidth > 0 && header -> bfType == 0x4D42)
        return true;
    return false;
}

static uint8_t padding(uint32_t image_width) {
    uint8_t pad = (image_width * sizeof(struct pixel)) % 4;

    if(pad != 0) return 4 - pad;
    return pad;
}

static bool from_pix(FILE* in_file, struct image* image, struct bmp_header header) {

    if(fseek(in_file, header.bOffBits, SEEK_SET) != 0) {
        return false;
    }

    uint32_t bits = 0;
    for (size_t i = 0; i < image -> height; i++) {
        
        bits = fread(&(image -> data[i * (image -> width)]), 
        sizeof(struct pixel), image -> width, in_file);

        if(bits != image -> width) { 
            return false;
        }
        if(fseek(in_file, padding(image -> width), SEEK_CUR) != 0) {
            return false;
        }
    }
    return true;
}

static bool to_pix(FILE* out_file, struct image const* image) {
    const uint8_t pad = padding(image -> width);

    const uint8_t garbage[3] = {0, 0, 0};

    if(fseek(out_file, sizeof(struct bmp_header), SEEK_SET) != 0) {
        return false;
    };

    for (size_t i = 0; i < image -> height; i++){
        uint32_t items = fwrite(image -> data + (image -> width * i),
                                        sizeof(struct pixel), image -> width, out_file);

        if (items != image -> width){
            return false;
        }

        uint32_t garbage_written = fwrite(garbage, sizeof(uint8_t), pad, out_file);

        if (garbage_written != pad) return false;
    }
    return true;
}

bool from_bmp(FILE* to_file, struct image* image) {
    struct bmp_header header = {0};

    if(!to_file || !image) return false;

    if (fread(&header, sizeof(struct bmp_header),
              1, to_file) != 1) return false;

    if(!is_valid(&header)) return false;

    if(!image_malloc(image, header.biWidth, header.biHeight)) return false;

    return from_pix(to_file, image, header);
}

bool to_bmp(FILE* out_file, struct image const* image) {

    if(!out_file || !image) {
        return false;
    }

    struct bmp_header header = create_header(image);

    if (fwrite(&header, sizeof(struct bmp_header),
               1, out_file) != 1) {
        return false;
    }

    return to_pix(out_file, image);
}
