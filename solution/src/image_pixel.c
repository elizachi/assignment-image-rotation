#include "../include/image_pixel.h"
#include <malloc.h>
#include <stdbool.h>

bool image_malloc(struct image *image, uint32_t width, uint32_t height) {

    struct pixel *pix = malloc(width * height * sizeof(struct pixel));

    if (pix == NULL){
        free(pix);
        return false;
    }

    image -> width = width;
    image -> height = height;
    image -> data = pix;

    return true;
}

void image_free(struct image *image) {
    free(image -> data);
}
